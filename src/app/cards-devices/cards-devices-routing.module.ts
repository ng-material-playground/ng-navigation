import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CardsDevicesComponent} from './cards-devices/cards-devices.component';

const routes: Routes = [{
  path: '',
  component: CardsDevicesComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CardsDevicesRoutingModule { }
