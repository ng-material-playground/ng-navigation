import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CardsDevicesRoutingModule } from './cards-devices-routing.module';
import { CardsDevicesComponent } from './cards-devices/cards-devices.component';

@NgModule({
  declarations: [CardsDevicesComponent],
  imports: [
    CommonModule,
    CardsDevicesRoutingModule
  ]
})
export class CardsDevicesModule { }
