import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-sidebar-item',
  templateUrl: './sidebar-item.component.html',
  styleUrls: ['./sidebar-item.component.css']
})
export class SidebarItemComponent implements OnInit {

  @Input('iconName') iconName: string;
  @Input('title') title: string;
  @Input('routeName') routeName: string;

  @Output('navigate') navigate = new EventEmitter();

  constructor() {}

  ngOnInit() {
  }

  onNavigate() {
    this.navigate.emit();
  }

}
