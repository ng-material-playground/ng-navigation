import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  @Input('width') windowWidth;
  @Output('onNavigate') sidenavClick = new EventEmitter();

  isOpen: boolean;

  expandHeight = '42px';
  collapseHeight = '42px';
  displayMode = 'flat';

  ngOnInit() {
    this.isOpen = false;
  }

  constructor() {}

  emitNavigate() {
    this.sidenavClick.emit(true);
  }
}
