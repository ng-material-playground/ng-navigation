import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule} from '@angular/router';

import {
  MatButtonModule,
  MatExpansionModule,
  MatIconModule,
  MatListModule,
  MatMenuModule,
  MatSidenavModule,
  MatToolbarModule
} from '@angular/material';

import { SidebarComponent } from './layout/sidebar/sidebar.component';
import { SidebarItemComponent } from './layout/sidebar-item/sidebar-item.component';
import { ToolbarComponent } from './layout/toolbar/toolbar.component';
import { ContentComponent } from './layout/content/content.component';


@NgModule({
  declarations: [
    SidebarComponent,
    SidebarItemComponent,
    ToolbarComponent,
    ContentComponent,
    ],
  imports: [
    CommonModule,
    RouterModule,

    MatToolbarModule,
    MatSidenavModule,
    MatExpansionModule,
    MatListModule,
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
  ],
  exports: [
    CommonModule,
    RouterModule,

    SidebarComponent,
    ToolbarComponent,
    ContentComponent,

    MatToolbarModule,
    MatSidenavModule,
    MatExpansionModule,
    MatListModule,
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
  ]
})
export class SharedModule { }
