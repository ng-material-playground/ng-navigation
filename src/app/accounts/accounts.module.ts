import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountsRoutingModule } from './accounts-routing.module';
import { AccountListComponent } from './account-list/account-list.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [AccountListComponent],
  imports: [
    CommonModule,

    SharedModule,
    AccountsRoutingModule
  ]
})
export class AccountsModule { }
